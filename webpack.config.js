const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
      path: path.join(__dirname, 'dist'),
      filename: 'bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader'
          }
        ]
      },
      {
          test: /\.css$/,
          use: [
              {
                  loader: MiniCssExtractPlugin.loader
              },
              'css-loader'
          ]
      }
    ]
  },
  plugins: [

    //Dynamci HTML generation
    new HtmlWebPackPlugin({
      template: './public/index.html',
      inject: true
    }),

    //CSS
    new MiniCssExtractPlugin({
        filename: 'bundle.css'
    }),

    //Manifest and icons
    new CopyPlugin([
        {
            from: './public/',
            to: path.join(__dirname, 'dist'),
            ignore: ['*.html'],
        },
        {
            from: './public/icons',
            to: 'icons'
        }
      ])
  ]
};
